# pare
`pare` - standing for **pa**rser-**re**actor is a library designed to alleviate many of the problems currently facing rust io - in particular, the difficulties in sync vs async, the difficulties in different async runtimes, and some difficulties in using these on embedded platforms.

It is based partially on the ideas espoused in [the sans-io site](https://sans-io.readthedocs.io/how-to-sans-io.html), but adapted to Rust with zero-cost abstractions and compile-type encoding. It is designed to avoid heap allocations as much as possible, with a focus on embedded. It is also - ideally - usable to create parsers with predictable memory usage.

Eventual goals for this project include integration with `nom` and `serde` to aid in the use of existing parsers, and similar such things.

## Core Ideas
`pare` is built on the idea of driving a parser with input, in which the input may not be complete, and emitting more advanced structures when they are finished and reacting to them - either reacting to them with some kind of processing event, or using them to build a larger object.

Fundamentally, the idea is that we can *provide partial input* and modify a parser state machine until it produces a value, independent of the means of obtaining that input.

## Architecture
Rust already has a highly performant way of defining interruptable state machines that efficiently carry around necessary state - `async` and `Future`. Typically, these come attached with a runtime and *executor* - such as `tokio` or `async_std`. In particular, the executor drives the futures forward by calling a function called `poll`, that indicates if the future has output or is still computing.

This means that, currently, the means of managing io and other operations is attached to the runtime specifically, as that is the only thing that is able to resume the execution of a waiting future, in the form of the executor.

However, if we provide a means of driving input (and producing output) to the user, and then a unified interface to *receive* input or *provide* output to a state machine defined by a future, that can then be driven externally either synchronously or asynchronously, we can avoid depending on a specific runtime like `tokio` or `async-std` or perhaps the standard library, so users just handle receiving input and feeding it to the state machine defined by the inner, `pare` futures.

Output is similar, just in reverse - you provide whatever is needed to drive a future forward, and receive output as the state machine advances, and then send that output off using whatever runtime you are using.

## Feeding the Futures
The main difficulty, at this point, is *providing a future with new data and receiving data from it*. In particular the `Poll` structure returned by futures provides no easy way to add extra arguments or a return value when the future is not `Ready`. 

However, there is another option. Embed an intermediary context variable in the [`core::future::Future`] that is passed down from the top-level state machine through to the most basic component, as a mutable reference, and then modify it in-place. Essentially, an embedded inout argument.
