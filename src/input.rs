//! Module that contains an API for obtaining *input* from some io source.

/// Contains useful inports
pub mod prelude {
    pub use super::{atomic::prelude::*, partial::prelude::*};
}

/// Atomic input operations.
///
/// These *either* consume all the contiguous input they need, or fail and consume *nothing*,
/// providing an indication of how much more contiguous input they would need for the next call to
/// have a chance to complete.
///
/// Because atomic operations either have enough input, or don't have enough input, it means that
/// they don't need to worry about storing intermediary state, and can be zero-copy as they never
/// have to worry about the lifetimes of their inputs expiring while paused (they can't pause,
/// after all).
///
/// This module contains types and traits to aid in creating those atomic operations. These are
/// prefixed with `IA` (for *input*, *atomic*), for easy mixed-module importing.
///
/// ### Combining atomic operations
/// The traits and structures in this module are designed for creating composable, modular atomic
/// operations that can be easily combined into larger atomic operations as desired.
///
/// ### Example atomic operation definitions!
///
/// Something to extract 3 [`u32`]s, big endian, from a slice of at least 12 bytes, atomically:
/// ```rust
/// use pare::input::atomic::prelude::*;
///
/// /// Obtain 3 contiguous [`u32`]s from the start of a collection of bytes.
/// pub fn ia_u32_big_endian_triplet(c: IAConsumed<'_, u8>) -> IAResult<'_, u8, [u32;3]> {
///     let (first_bytes, c) = ia_take_fixed::<u8, 4>(c)?;
///     let (second_bytes, c) = ia_take_fixed::<u8, 4>(c)?;
///     let (third_bytes, c) = ia_take_fixed::<u8, 4>(c)?;
///     Ok(([first_bytes, second_bytes, third_bytes].map(u32::from_be_bytes), c))
/// }
///
/// let without_enough_input = ia_u32_big_endian_triplet(IAConsumed::new(&[0x4u8, 0x11, 0xFF, 0x37]));
/// // The failure only indicates *4* more is needed to make progress, because there is no good way
/// // to merge with future statements. Once 4 more units of input are provided, it will again need
/// // more.
/// assert_eq!(without_enough_input, Err(IAFailed { needs: 4.try_into().unwrap(), suggested: 0 }) );
///
/// let with_extra_input = ia_u32_big_endian_triplet(IAConsumed::new(&[
///     0xFF, 0xFF, 0xFF, 0xFF,
///     0x11, 0x11, 0x11, 0x11,
///     0x54, 0x32, 0x10, 0xFF,
///     0xEE, 0xEE
/// ]));
/// // This time, there was a lot more input than necessary, and we get the extra back as something
/// // that can then be passed to further parsers.
/// // This is probably reminding you of `nom` parser-combinators, and in fact, atomic operations
/// // in this crate are essentially those!
/// assert_eq!(with_extra_input, Ok((
///     [0xFFFFFFFFu32, 0x11111111, 0x543210FF],
///     IAConsumed { ate: 12, remaining: &[0xEE, 0xEE]} ))
/// );
///
/// ```
///
pub mod atomic {
    use chain_trans::prelude::*;
    use core::{num::NonZeroUsize, pin::Pin};

    pub mod prelude {
        pub use super::{
            ia_take_dyn_preprocessed, ia_take_dyn_referenced, ia_take_fixed, ia_take_fixed_cloned,
            ia_take_fixed_copy_preprocessed, ia_take_fixed_owned, ia_take_fixed_preprocessed,
            ia_take_fixed_referenced, ia_take_one, ia_take_one_cloned,
            ia_take_one_copy_preprocessed, ia_take_one_owned, ia_take_one_preprocessed,
            ia_take_one_referenced, IAConsumed, IAFailed, IAOperationExt, IAResult,
        };
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    /// A successful result of an atomic operation.
    pub struct IAConsumed<'i, I> {
        /// Total amount all previous parts of this atomic operation consumed from the input.
        pub ate: usize,
        /// The input left after the operation.
        pub remaining: &'i [I],
    }

    impl<'i, I> IAConsumed<'i, I> {
        /// Create unconsumed input to start with, from raw contiguous input.
        #[inline]
        pub fn new(contiguous_input: &'i [I]) -> Self {
            IAConsumed {
                ate: 0,
                remaining: contiguous_input,
            }
        }

        /// Try and take a single component from the slice, failing if empty, with a preprocessing
        /// method.
        #[inline]
        pub fn take_one_preprocessed<Q>(
            &self,
            transform: impl FnOnce(&'i I) -> Q,
        ) -> IAResult<'i, I, Q> {
            match self {
                Self {
                    ate: _,
                    remaining: [],
                } => Err(IAFailed::new(1.try_into().expect("1 > 0"))),
                Self {
                    ate,
                    remaining: [first, rest @ ..],
                } => Ok((
                    transform(first),
                    Self {
                        ate: ate + 1,
                        remaining: rest,
                    },
                )),
            }
        }

        /// Take a dynamically sized chunk of data out of the slice.
        ///
        /// If you know `n` at compile-time, look at [`Self::take_fixed_n_preprocessed`] instead, to obtain an
        /// array.
        #[inline]
        pub fn take_n(&self, n: usize) -> IAResult<'i, I, &'i [I]> {
            match self {
                Self { ate: _, remaining } if remaining.len() < n => Err(IAFailed::new(
                    (n - remaining.len())
                        .try_into()
                        .expect("n > remaining.len()"),
                )),
                Self { ate, remaining } => {
                    remaining.split_at(n).trans(|(result_slice, remaining)| {
                        Ok((
                            result_slice,
                            IAConsumed {
                                ate: ate + n,
                                remaining,
                            },
                        ))
                    })
                }
            }
        }

        /// Try and take a fixed *N* of components in one go, with arbitrary preprocessing
        /// function.
        ///
        /// If you don't know N at compile time, take a look at [`Self::take_n`]
        #[inline]
        pub fn take_fixed_n_preprocessed<const N: usize, Q, Transform: Fn(&'i [I; N]) -> Q>(
            &self,
            preprocessor: Transform,
        ) -> IAResult<'i, I, Q> {
            self.take_n(N).map(|(slice, consumed)| {
                (
                    slice
                        .try_into()
                        .map(preprocessor)
                        .expect("inner function returns slice of length N"),
                    consumed,
                )
            })
        }
    }

    /// A failed component of an atomic operation. Atomic operations can't partially consume
    /// anything, and so they can upon failure only ever produce a suggestion of how much more
    /// *contiguous* input is needed such that the operation gets further along the next time it is
    /// called.
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct IAFailed {
        /// The extra input needed to make further progress through the operation.
        pub needs: NonZeroUsize,
        /// Suggestion for faster progress, on top of [`IAFailed::needs`]
        pub suggested: usize,
    }

    impl IAFailed {
        #[inline]
        /// Create a failure indicator without suggestion for accelerated progress.
        pub fn new(needs: NonZeroUsize) -> Self {
            Self {
                needs,
                suggested: 0,
            }
        }

        /// Fail, with a suggestion for faster progress.
        #[inline]
        pub fn with_suggestion(needs: NonZeroUsize, suggestion: usize) -> Self {
            Self {
                needs,
                suggested: suggestion,
            }
        }
    }

    /// The result of a single part of an atomic input operation
    /// Use `?` to rollback any atomic operation that fails halfway through.
    pub type IAResult<'i, I, T> = Result<(T, IAConsumed<'i, I>), IAFailed>;

    /// A trait providing arbitrary atomic operations for an input slice `&'buf [I]`.
    pub trait IAOperation<'buf, I> {
        /// The final result of this atomic operation if successful.
        type Output;

        /// Attempt to perform this component of the atomic operation, with information on the
        /// amount of previously consumed input provided as the only parameter. This should:
        /// * If successful, return the output and a new [`IAConsumed`] with remaining input and the
        ///   amount of input that was consumed added on to `previously_consumed.ate`
        /// * If failed, return [`IAFailed`] with information on how much extra input would have to
        ///   be provided before the inner function would make progress in succeeding i.e. such
        ///   that the current function doesn't fail at the same place.
        ///   This is designed with composability in mind - you can keep calling the function with
        ///   more and more input until it succeeds, with the failure providing input on how much
        ///   is required to have a chance of success.
        ///   You can also provide hints for making this input refinement faster.
        fn try_atomic_input(
            self: Pin<&mut Self>,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output>;
    }

    /// Implement [`IAOperation`] for all bare functions taking [`IAConsumed`] and producing
    /// [`IAResult`].
    impl<'buf, I: 'buf, Q, T: Fn(IAConsumed<'buf, I>) -> IAResult<'buf, I, Q>> IAOperation<'buf, I>
        for T
    {
        type Output = Q;

        #[inline]
        fn try_atomic_input(
            self: Pin<&mut Self>,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output> {
            self.into_ref().as_ref()(previously_consumed)
        }
    }

    /// Useful ease-of-use methods.
    pub trait IAOperationExt<'buf, I>: IAOperation<'buf, I> + Unpin {
        /// Perform an input operation
        ///
        /// See [`IAOperation::try_atomic_input`] for details.
        #[inline]
        fn take(
            &mut self,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output> {
            Pin::new(self).try_atomic_input(previously_consumed)
        }
    }

    impl<'buf, I, T: Unpin + IAOperation<'buf, I>> IAOperationExt<'buf, I> for T {}

    /// Atomic operation that takes a single unit of input as an [`IAOperation`], and
    /// preprocesses it.
    #[derive(Clone, Copy, Debug)]
    pub struct IATakeOnePreprocessed<Transform>(pub Transform);

    impl<'buf, I: 'buf, Q, Transform: Fn(&'buf I) -> Q> IAOperation<'buf, I>
        for IATakeOnePreprocessed<Transform>
    {
        type Output = Q;

        #[inline]
        fn try_atomic_input(
            self: Pin<&mut Self>,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output> {
            previously_consumed.take_one_preprocessed(&self.as_ref().0)
        }
    }

    /// Operation that takes a single element from some input, by reference.
    #[inline]
    pub fn ia_take_one_referenced<I>(c: IAConsumed<'_, I>) -> IAResult<'_, I, &'_ I> {
        IATakeOnePreprocessed(|a| a).take(c)
    }

    /// Operation that takes a single element from some input, and copies it
    #[inline]
    pub fn ia_take_one<I: Copy>(c: IAConsumed<'_, I>) -> IAResult<'_, I, I> {
        IATakeOnePreprocessed(Clone::clone).take(c)
    }

    /// Operation that takes a single element from some input, and *clones* it.
    #[inline]
    pub fn ia_take_one_cloned<I: Clone>(c: IAConsumed<'_, I>) -> IAResult<'_, I, I> {
        IATakeOnePreprocessed(Clone::clone).take(c)
    }

    /// Operation that takes a single element from some input, and takes ownership of it.
    #[inline]
    #[cfg(feature = "alloc")]
    pub fn ia_take_one_owned<I: alloc::borrow::ToOwned>(
        c: IAConsumed<'_, I>,
    ) -> IAResult<'_, I, <I as alloc::borrow::ToOwned>::Owned> {
        IATakeOnePreprocessed(alloc::borrow::ToOwned::to_owned).take(c)
    }

    /// Operation that generically preprocesses a single input reference
    #[inline]
    pub fn ia_take_one_preprocessed<I, Q, F: Fn(&'_ I) -> Q>(
        c: IAConsumed<'_, I>,
        preprocessor: F,
    ) -> IAResult<'_, I, Q> {
        // Shared references are always treated as unpin :)
        IATakeOnePreprocessed(&preprocessor).take(c)
    }

    /// Like [`ia_take_one_preprocessed`], but for the case where `I` is copy it allows by-value
    /// functions instead.
    #[inline]
    pub fn ia_take_one_copy_preprocessed<I: Copy, Q, F: Fn(I) -> Q>(
        c: IAConsumed<'_, I>,
        preprocessor: F,
    ) -> IAResult<'_, I, Q> {
        ia_take_one_preprocessed(c, |v| preprocessor(*v))
    }

    /// Take a (dynamic) number of items from input as an [`IAOperation`], with a preprocessor.
    #[derive(Clone, Copy, Debug)]
    pub struct IATakeDynPreprocessed<Transform>(pub usize, pub Transform);

    impl<'buf, I: 'buf, Q, Transform: Fn(&'buf [I]) -> Q> IAOperation<'buf, I>
        for IATakeDynPreprocessed<Transform>
    {
        type Output = Q;

        #[inline]
        fn try_atomic_input(
            self: Pin<&mut Self>,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output> {
            previously_consumed
                .take_n(self.0)
                .map(|(result, consumed)| (self.into_ref().get_ref().1(result), consumed))
        }
    }

    /// Take a dynamically-determined number of items from the input.
    ///
    /// Note that it impossible to do this kind of dynamic-sized operation in an
    /// owning manner (as in `ia_take_fixed` and similar) without a buffer or allocations
    /// - see [`crate::buf`] for information on buffers and their exact design and use in
    /// the development of partial operations from atomic operations.
    #[inline]
    pub fn ia_take_dyn_referenced<I>(c: IAConsumed<'_, I>, n: usize) -> IAResult<'_, I, &'_ [I]> {
        IATakeDynPreprocessed(n, |v| v).take(c)
    }

    /// Like [`ia_take_dyn_referenced`] but with a preprocessor function.
    #[inline]
    pub fn ia_take_dyn_preprocessed<I, Q, F: Fn(&'_ [I]) -> Q>(
        c: IAConsumed<'_, I>,
        n: usize,
        preprocessor: F,
    ) -> IAResult<'_, I, Q> {
        IATakeDynPreprocessed(n, &preprocessor).take(c)
    }

    /// Take a (compile time) *N* items from input into an array reference, as an
    /// [`IAOperation`], with a preprocessor function.
    pub struct IATakeFixedPreprocessed<const N: usize, Transform>(pub Transform);

    impl<'buf, I: 'buf, const N: usize, Q, Transform: Fn(&'buf [I; N]) -> Q> IAOperation<'buf, I>
        for IATakeFixedPreprocessed<N, Transform>
    {
        type Output = Q;

        #[inline]
        fn try_atomic_input(
            self: Pin<&mut Self>,
            previously_consumed: IAConsumed<'buf, I>,
        ) -> IAResult<'buf, I, Self::Output> {
            previously_consumed.take_fixed_n_preprocessed::<N, _, _>(&self.as_ref().get_ref().0)
        }
    }

    /// Operation that takes a single element from some input, by reference.
    #[inline]
    pub fn ia_take_fixed_referenced<I, const N: usize>(
        c: IAConsumed<'_, I>,
    ) -> IAResult<'_, I, &'_ [I; N]> {
        IATakeFixedPreprocessed(|a| a).take(c)
    }

    /// Operation that takes *N* elements from some input, and copies them
    #[inline]
    pub fn ia_take_fixed<I: Copy, const N: usize>(c: IAConsumed<'_, I>) -> IAResult<'_, I, [I; N]> {
        IATakeFixedPreprocessed(Clone::clone).take(c)
    }

    /// Operation that takes *N* elements from some input, and clones them.
    #[inline]
    pub fn ia_take_fixed_cloned<I: Clone, const N: usize>(
        c: IAConsumed<'_, I>,
    ) -> IAResult<'_, I, [I; N]> {
        IATakeFixedPreprocessed(Clone::clone).take(c)
    }

    /// Operation that takes N elements from some input, and takes ownership of them
    /// using [`alloc::borrow::ToOwned`]
    ///
    /// Note that due to limitations in the api of arrays, this requires that `I` be [`Clone`],
    /// until `each_ref` is stabilised
    ///
    /// TODO: When each_ref is stabilised, use that and remove the `Clone` restriction on `I`
    #[inline]
    #[cfg(feature = "alloc")]
    pub fn ia_take_fixed_owned<I: alloc::borrow::ToOwned + Clone, const N: usize>(
        c: IAConsumed<'_, I>,
    ) -> IAResult<'_, I, [<I as alloc::borrow::ToOwned>::Owned; N]> {
        IATakeFixedPreprocessed(|arr: &[I; N]| {
            arr.clone()
                .map(|inner| alloc::borrow::ToOwned::to_owned(&inner))
        })
        .take(c)
    }

    /// Operation that generically preprocesses *N* input references
    #[inline]
    pub fn ia_take_fixed_preprocessed<I, const N: usize, Q, F: Fn(&'_ [I; N]) -> Q>(
        c: IAConsumed<'_, I>,
        preprocessor: F,
    ) -> IAResult<'_, I, Q> {
        // & here to ensure no need to worry about pinning :)
        IATakeFixedPreprocessed(&preprocessor).take(c)
    }

    /// Like [`ia_take_fixed_preprocessed`], but for the case where `I` is copy it allows by-value
    /// functions instead.
    #[inline]
    pub fn ia_take_fixed_copy_preprocessed<I: Copy, const N: usize, Q, F: Fn([I; N]) -> Q>(
        c: IAConsumed<'_, I>,
        preprocessor: F,
    ) -> IAResult<'_, I, Q> {
        ia_take_fixed_preprocessed(c, |v| preprocessor(*v))
    }
}

/// Unlike [`atomic`] input processors, items in this module can make *partial* progress towards the production of
/// a value, consuming some amount of contiguous input in chunks with intermediary information
/// stored inside state.
///
/// Items in this module are prefixed with `IP` (for *input*, *partial*), for importing alongside
/// other modules with a uniform API.
///
/// Partial parsers cannot be *zero copy* like [`atomic`] parsers. This is because the lifetime of
/// any given chunk of contiguous input has to be shorter than the stored intermediary state (in
/// particular, because the intermediary state has to be modified to add input if there was
/// insufficient input, which would be the primary cause of pauses!).
///
/// This also means that if you want to avoid allocations, you can't store dynamically sized types
/// across breakpoints. With [`atomic`] input parsers, you can extract dynamically-sized chunks of
/// input by holding, say, a reference to a slice (see [`atomic::ia_take_dyn_referenced`]).
///
/// However, because the produced types must live longer than the input buffer with [`partial`] parsers,
/// you have to own the type. Stack allocation requires that the type be sized, so you can't own a bare `[T]` -
/// instead you are forced to allocate [`::std::vec::Vec`] or similar. Or, as is a preferred
/// method, use the types defined in [`crate::buf`] and turn atomic operations into partial
/// operations even for dynamic length types.
///
/// Partial parsers have a couple of advantages even without allocating:
/// * Being able to accept almost arbitrarily small input chunks, so you can use small buffers
///   and just take whatever input is available rather than worrying about queuing and shuffling
///   out old input.
///   This also lets you effectively create streaming input parsers and reactors.
/// * Statically determined memory usage of the parser (as long as you don't involve memory
///   allocation).
///
/// Generally speaking, you should *prefer* partial parsers in almost all cases, unless dealing
/// with big chunks of data that need zero-copy. Tools are provided to combine a partial parser and
/// an [`atomic`] parser - see [`crate::buf`]
///
/// Partial parsers use [`crate::async_state`] pollstate futures to enable the ergonomic
/// construction of parser-reactor state machines.
pub mod partial {
    use arrayvec::ArrayVec;
    use core::{marker::PhantomData, num::NonZeroUsize, pin::Pin};

    /// Useful-to-include types and traits
    pub mod prelude {
        pub use super::{
            // Fixed len operations
            ip_take_fixed,
            ip_take_fixed_cloned,
            ip_take_fixed_owned,
            ip_take_fixed_preprocessed,

            // Functions for creating operations and futures
            ip_take_one,
            ip_take_one_cloned,
            ip_take_one_owned,
            ip_take_one_preprocessed,

            // Pollstate prelude
            pollstate::prelude::*,
            // function for gluing state onto data.
            IPOperationExt,
        };
    }

    /// Like [`super::atomic::IAConsumed`].
    ///
    /// Documents how much of an input was consumed, and the amount left over.
    ///
    /// With partial operations, this gets a bit more complicated, because the input can be
    /// modified during interruptions. Partial operations are defined using stateful futures also,
    /// which means that these get fed into that machine rather than passed around by the end user
    /// of an input parser-reactor.
    ///
    /// Therefore, we say that consumptions here are for within each movement through the state
    /// machine of input processing - they document how much of the *current* input has been
    /// consumed in the *current* `poll` or equivalent partial parsing attempt.
    #[derive(Debug)]
    pub struct IPConsumed<'buf, I> {
        /// Total amount all previous parts of this current poll have consumed from the input.
        pub ate: usize,
        /// The input left at the end of this current poll operation.
        pub remaining: &'buf [I],
    }

    // Manual impl so theres no Copy bound on I
    impl<'buf, I> Copy for IPConsumed<'buf, I> {}
    impl<'buf, I> Clone for IPConsumed<'buf, I> {
        #[inline]
        fn clone(&self) -> Self {
            *self
        }
    }

    impl<'buf, I> IPConsumed<'buf, I> {
        /// Async method of taking a single item. This is what is used to build all of the *other*
        /// things, as it only ever needs one thing to progress each time. Provides a means of
        /// preprocessing the input for ownership.
        ///
        /// Note that, unlike [`super::atomic::IAConsumed::take_one_preprocessed`], this has to work without
        /// holding a reference to the buffer of `I` in the result. This means that we require a
        /// transform that can take a reference of any lifetime and produce an output.
        ///
        /// For [`Copy`] and [`Clone`] types, take a look at [`Self::take_one`] and
        /// [`Self::take_one_cloned`] for simple APIs
        #[inline]
        pub fn take_one_preprocessed<Q>(
            &self,
            ownify: impl for<'b> FnOnce(&'b I) -> Q,
        ) -> IPResult<'buf, I, Q> {
            match self {
                c @ IPConsumed {
                    ate: _,
                    remaining: [],
                } => Err(IPFailed::new(*c, 1.try_into().expect("nonzero"))),
                IPConsumed {
                    ate,
                    remaining: [res, rest @ ..],
                } => Ok((
                    ownify(res),
                    IPConsumed {
                        ate: ate + 1,
                        remaining: rest,
                    },
                )),
            }
        }

        /// Async method of taking a single item. This is what is used to build all of the *other*
        /// things, as it only ever needs one thing to progress each time.   
        ///
        /// Note that, unlike [`super::atomic::IAConsumed::take_one_preprocessed`], this has to work without
        /// holding a reference to the buffer of `I` in the result. This means that we require
        /// `I` to implement [`Copy`]. It delegates to [`Self::take_one_preprocessed`]
        #[inline]
        pub fn take_one(&self) -> IPResult<'buf, I, I>
        where
            I: Copy,
        {
            self.take_one_preprocessed(Clone::clone)
        }

        /// Like [`IPConsumed::take_one`], but clones the input if necessary
        #[inline]
        pub fn take_one_cloned(&self) -> IPResult<'buf, I, I>
        where
            I: Clone,
        {
            self.take_one_preprocessed(Clone::clone)
        }

        /// Like [`IPConsumed::take_one_cloned`], but using [`alloc::borrow::ToOwned`] instead.
        #[inline]
        #[cfg(feature = "alloc")]
        pub fn take_one_owned(&self) -> IPResult<'buf, I, <I as alloc::borrow::ToOwned>::Owned>
        where
            I: alloc::borrow::ToOwned,
        {
            self.take_one_preprocessed(alloc::borrow::ToOwned::to_owned)
        }
    }

    /// Like [`super::atomic::IAFailed`]
    ///
    /// Documents how much more input than the current *unconsumed* amount of input is needed to
    /// make progress when the current poll operation is attempted again.
    ///
    /// Because partial parsers can consume some amount of input even when the parser was
    /// incomplete, this embeds how much of the input has been consumed during the current attempt
    /// at parsing more input.
    #[derive(Debug)]
    pub struct IPFailed<'buf, I> {
        /// How much input was consumed (and how much is left over) in the current attempt at
        /// reaching completion.
        pub consumed: IPConsumed<'buf, I>,
        /// The minimum extra contiguous input beyond [`IPConsumed::remaining`] to make progress
        /// towards completion.
        pub needs: NonZeroUsize,
        /// A suggested amount of extra input to make better progress towards completion (added
        /// onto the minimum amount needed) - for instance, a partial parser for a `[I;N]` array
        /// may only *need* one chunk of input at a time to make progress, but it can go faster if
        /// provided with `N-current_amount-1` units of input on top of the minimum of 1 input at a
        /// time.
        pub suggested: usize,
    }

    // Manual implementation to avoid bound on `I`
    impl<'buf, I> Copy for IPFailed<'buf, I> {}
    impl<'buf, I> Clone for IPFailed<'buf, I> {
        #[inline]
        fn clone(&self) -> Self {
            *self
        }
    }

    impl<'buf, I> IPFailed<'buf, I> {
        /// Create a failure indicator without suggestion for accelerated progress, indicating how
        /// much was consumed by the partial process already.
        #[inline]
        pub fn new(consumed: IPConsumed<'buf, I>, needs: NonZeroUsize) -> Self {
            Self {
                needs,
                suggested: 0,
                consumed,
            }
        }

        /// Fail, with a suggestion for faster progress, indicating how much was consumed by the
        /// partial operation already.
        #[inline]
        pub fn with_suggestion(
            consumed: IPConsumed<'buf, I>,
            needs: NonZeroUsize,
            suggestion: usize,
        ) -> Self {
            Self {
                needs,
                suggested: suggestion,
                consumed,
            }
        }
    }

    /// Represents the intermediary - in case of Err - or final - in case of Ok - state of a
    /// partial parser providing of input.
    pub type IPResult<'buf, I, Output> = Result<(Output, IPConsumed<'buf, I>), IPFailed<'buf, I>>;

    /// Represents an arbitrary partial input parser operation.
    ///
    /// Note that unlike [`super::atomic::IAOperation`], this must work for
    /// arbitrary-lifetime input buffers, because this must be able to pause, and it can't store
    /// the input buffers anywhere at all.
    ///
    /// These are intrinsically stateful. The user of this operation has no idea how much progress
    /// has already been made in the operation when new input is provided to the function. *You*
    /// have to manage that part - In many ways this is like a [`core::future::Future`]
    pub trait IPOperation<I> {
        /// The finally produced type by this operation.
        type Output;

        /// Attempt to make progress in this operation.
        ///
        /// This function either returns [`Ok`] - indicating the operation has completed - or it
        /// returns [`Err`], which provides information on how much input was consumed, and how
        /// much is needed to make further progress, and optionally a suggestion for accelerated
        /// progress.
        ///
        /// After this function returns [`Ok`], the caller must never call this function again,
        /// with the same criteria as on [`poll`][poll]
        ///
        /// [poll]: core::future::Future::poll
        fn try_progress_partial_input<'buf>(
            self: Pin<&mut Self>,
            previous_operation_chunk: IPConsumed<'buf, I>,
        ) -> IPResult<'buf, I, Self::Output>;
    }

    /// Extension trait for [`IPOperation`]
    pub trait IPOperationExt<I>: IPOperation<I> {
        #[inline]
        /// Create a future with the given input state
        fn with_istate<ICtx: pollstate::IContext<I>>(
            self,
            state: &ICtx,
        ) -> pollstate::IPOperationFuture<'_, I, ICtx, Self>
        where
            Self: Sized,
        {
            pollstate::IPOperationFuture::new(state, self)
        }
    }

    impl<I, T: IPOperation<I>> IPOperationExt<I> for T {}

    /// Take a single item, in an interruptible/partial manner, when the item is copyable.
    ///
    /// Function is the transform that occurs to make the item become *owned*.
    #[derive(Debug, Copy, Clone)]
    pub struct IPTakeOnePreprocessed<Ownify>(pub Ownify);

    impl<I, Q, Ownify: for<'b> Fn(&'b I) -> Q> IPOperation<I> for IPTakeOnePreprocessed<Ownify> {
        type Output = Q;

        #[inline]
        fn try_progress_partial_input<'buf>(
            self: Pin<&mut Self>,
            previous_operation_chunk: IPConsumed<'buf, I>,
        ) -> IPResult<'buf, I, Self::Output> {
            previous_operation_chunk.take_one_preprocessed(&self.into_ref().get_ref().0)
        }
    }

    /// Create an operation that takes a single copy type.
    #[inline]
    pub fn ip_take_one<I: Copy>() -> IPTakeOnePreprocessed<fn(&I) -> I> {
        IPTakeOnePreprocessed(Clone::clone)
    }

    /// Create an operation that takes a single clone type.
    #[inline]
    pub fn ip_take_one_cloned<I: Clone>() -> IPTakeOnePreprocessed<fn(&I) -> I> {
        IPTakeOnePreprocessed(Clone::clone)
    }

    /// Create an operation that takes ownership of the input types
    #[inline]
    #[cfg(feature = "alloc")]
    pub fn ip_take_one_owned<I: alloc::borrow::ToOwned>(
    ) -> IPTakeOnePreprocessed<fn(&I) -> I::Owned> {
        use alloc::borrow::ToOwned;
        IPTakeOnePreprocessed(ToOwned::to_owned)
    }

    /// Perform a single operation with a custom function for taking ownership of input values.
    #[inline]
    pub fn ip_take_one_preprocessed<I, Q, F: for<'b> Fn(&'b I) -> Q>(
        ownify: F,
    ) -> IPTakeOnePreprocessed<F> {
        IPTakeOnePreprocessed(ownify)
    }

    /// Take a fixed number of items into an array, with the provided preprocessor.
    #[derive(Debug, Clone)]
    pub struct IPTakeFixedPreprocessed<I, Q, Ownify: Fn(&I) -> Q, const N: usize> {
        data: ArrayVec<Q, N>,
        ownify: Ownify,
        _pd: PhantomData<[I]>,
    }

    impl<I, Q, Ownify: Fn(&I) -> Q, const N: usize> IPTakeFixedPreprocessed<I, Q, Ownify, N> {
        #[inline]
        /// Create an emppty preprocessor of self.
        pub fn new(ownify: Ownify) -> Self {
            Self {
                data: Default::default(),
                ownify,
                _pd: PhantomData,
            }
        }

        #[inline]
        /// Project out the pinnedness of stuff. Note that the array vector is not pinned at all
        /// and so can never be returned from here as pinned. This is because it being [`Unpin`]
        /// requires `Q: Unpin` and that would be annoying to deal with.
        pub fn pin_project(self: Pin<&mut Self>) -> (&mut ArrayVec<Q, N>, &Ownify) {
            let bareself = unsafe { self.get_unchecked_mut() };
            (&mut bareself.data, &bareself.ownify)
        }
    }

    impl<I, Q, Ownify: Fn(&I) -> Q, const N: usize> IPOperation<I>
        for IPTakeFixedPreprocessed<I, Q, Ownify, N>
    {
        type Output = [Q; N];

        #[inline]
        fn try_progress_partial_input<'buf>(
            self: Pin<&mut Self>,
            previous_operation_chunk: IPConsumed<'buf, I>,
        ) -> IPResult<'buf, I, Self::Output> {
            let (state, ownify) = self.pin_project();
            // Amount needed to fill up the array.
            let needs = state.remaining_capacity();
            let IPConsumed { ate, remaining } = previous_operation_chunk;
            if remaining.len() >= needs {
                // We can consume all of the stuff.
                let (will_finish_array, remaining) = remaining.split_at(needs);
                state.extend(will_finish_array.iter().map(ownify));
                debug_assert!(state.is_full());
                Ok((
                    state
                        .take()
                        .into_inner()
                        .map_err(|_| unreachable!())
                        .expect("we just established that enough elements exist in the slice"),
                    IPConsumed {
                        ate: ate + needs,
                        remaining,
                    },
                ))
            } else {
                // We are after all going to take *everything*.
                let ate = ate + remaining.len();
                // Not enough to fill out the array.
                // Take as much as possible, and then indicate how much we *need* to progress (just
                // one element), and how much we *want* (enough to complete).
                state.extend(remaining.iter().map(ownify));
                let to_complete = state.remaining_capacity();
                Err(IPFailed::with_suggestion(
                    IPConsumed {
                        ate,
                        remaining: &[],
                    },
                    1.try_into().unwrap(),
                    to_complete - 1,
                ))
            }
        }
    }

    /// Create an operation that takes a single copy type, a fixed # of times
    #[inline]
    pub fn ip_take_fixed<I: Copy, const N: usize>() -> IPTakeFixedPreprocessed<I, I, fn(&I) -> I, N>
    {
        IPTakeFixedPreprocessed::new(Clone::clone)
    }

    /// Create an operation that takes a single clone type, a fixed # of times.
    #[inline]
    pub fn ip_take_fixed_cloned<I: Clone, const N: usize>(
    ) -> IPTakeFixedPreprocessed<I, I, fn(&I) -> I, N> {
        IPTakeFixedPreprocessed::new(Clone::clone)
    }

    /// Create an operation that takes ownership of the input types, a fixed # of times.
    #[inline]
    #[cfg(feature = "alloc")]
    pub fn ip_take_fixed_owned<I: alloc::borrow::ToOwned, const N: usize>(
    ) -> IPTakeFixedPreprocessed<I, I::Owned, fn(&I) -> I::Owned, N> {
        use alloc::borrow::ToOwned;
        IPTakeFixedPreprocessed::new(ToOwned::to_owned)
    }

    /// Perform a fixed # of operations with a custom function for taking ownership of input values.
    ///
    /// Note that, unlike [`super::atomic::ia_take_fixed_preprocessed`], this one instead uses a
    /// function that transforms each element individually. This is because - being a *partial*
    /// operation - the function must not need the entire array to transform it's inputs into an
    /// owning state.
    #[inline]
    pub fn ip_take_fixed_preprocessed<I, const N: usize, Q, F: for<'b> Fn(&'b I) -> Q>(
        ownify: F,
    ) -> IPTakeFixedPreprocessed<I, Q, F, N> {
        IPTakeFixedPreprocessed::new(ownify)
    }

    /// Module for managing the state passed around through [pollstate futures][async-state], for
    /// their use in defining state machines for input operations, and for doing so *safely*.
    ///
    /// This module defines:
    /// * A trait for providing lifetime-erased versions of [`IPConsumed`], [`IPFailed`], and
    /// [`IPResult`], which can be lifetime-unerased for controlled scopes in a safe manner.
    /// * Actual implementations of the lifetime erased versions of those.
    ///
    /// To provide custom contexts, have a look at [`pollstate::IContext`]
    ///
    /// [async-state]: `crate::async_state`
    pub mod pollstate {
        use chain_trans::Trans;

        use super::{IPConsumed, IPFailed, IPResult};
        use core::{future::Future, task::Poll};
        use core::{marker::PhantomData, num::NonZeroUsize, pin::Pin};

        pub mod prelude {}

        pub struct LtErasedIPConsumed<I> {
            ate: usize,
            // The slice, with it's lifetime erased.
            remaining: *const [I],
        }

        impl<I> Unpin for LtErasedIPConsumed<I> {}

        /// This is conceptually an `&[I]`, so it is [`Send`] with the same requirements.
        ///
        /// Updates to this item are not synchronised however, so it isn't [`Sync`] (
        unsafe impl<I> Send for LtErasedIPConsumed<I> where for<'a> &'a [I]: Send {}

        impl<I> LtErasedIPConsumed<I> {
            #[inline]
            /// Erase the lifetime of the given record of consumption and input slice.
            ///
            /// # Safety
            /// To use this safely, ensure that the contents of this are not accessed outside the lifetime of the original [`IPConsumed`]
            /// In particular, this is easiest to do if your only access to this value is between a
            /// call to [`Self::erase_lifetime`] and [`Self::unerase_lifetime`] in the same scope, using
            /// safe-update functions to help.
            pub unsafe fn erase_lifetime(c: IPConsumed<'_, I>) -> Self {
                Self {
                    ate: c.ate,
                    remaining: c.remaining,
                }
            }

            #[inline]
            /// Reprovide the lifetime to this lifetime-erased structure.
            ///
            /// # Safety
            /// see [`Self::erase_lifetime`] for tips on how to use this safely.
            pub unsafe fn unerase_lifetime<'buf>(self) -> IPConsumed<'buf, I> {
                IPConsumed {
                    ate: self.ate,
                    // SAFETY: upholding of contract
                    remaining: unsafe { &*self.remaining },
                }
            }
        }

        /// Like an [`IPFailed`] but with the lifetime deleted.
        pub struct LtErasedIPFailed<I> {
            consumed: LtErasedIPConsumed<I>,
            needs: NonZeroUsize,
            suggested: usize,
        }

        impl<I> LtErasedIPFailed<I> {
            #[inline]
            /// Erase the lifetime of the given record of a failure to complete an operation fully,
            /// including how much was successfully consumed.
            ///
            /// # Safety
            /// This has the same safety criterion as [`LtErasedIPConsumed::erase_lifetime`]
            pub unsafe fn erase_lifetime(f: IPFailed<'_, I>) -> Self {
                Self {
                    // Safety: upholding of function safety contract
                    consumed: unsafe { LtErasedIPConsumed::erase_lifetime(f.consumed) },
                    needs: f.needs,
                    suggested: f.suggested,
                }
            }

            #[inline]
            /// Reprovide the lifetime of this structure.
            ///
            /// # Safety
            /// See [`LtErasedIPConsumed::unerase_lifetime`] for details on how to use this kind of
            /// lifetime erasure safely.
            pub unsafe fn unerase_lifetime<'lt>(self) -> IPFailed<'lt, I> {
                IPFailed {
                    // safety: contract is upheld.
                    consumed: unsafe { self.consumed.unerase_lifetime::<'lt>() },
                    needs: self.needs,
                    suggested: self.suggested,
                }
            }
        }

        /// Lifetime-erased version of [`IPResult`], using [`LtErasedIPConsumed`] and
        /// [`LtErasedIPFailed`]. This does not store the output.
        pub type LtErasedIPResult<I> = Result<LtErasedIPConsumed<I>, LtErasedIPFailed<I>>;

        /// Structure for using [`LtErasedIPResult`] in a safe manner, that requires unsafe only
        /// for the people constructing this state rather than the people modifying it and changing
        /// it around, by restricting safe modification to lifetime-independent modifications and
        /// using [`Option`].
        ///
        /// This can't hold the output result, though. It only holds the intermediary input and
        /// failure states.
        ///
        /// This is suitable for use in pollstate futures with [`core::cell::Cell`]. It uses an
        /// [`Option`] internally, so if you take the lifetime erased state from a cell and don't return it it
        /// can cause emptiness internally.
        pub struct SafeIPResultPollstate<I> {
            inner_state: Option<LtErasedIPResult<I>>,
        }

        impl<I> Default for SafeIPResultPollstate<I> {
            #[inline]
            fn default() -> Self {
                Self { inner_state: None }
            }
        }

        impl<I> SafeIPResultPollstate<I> {
            #[inline]
            /// Erase the lifetime of an *initial* state. This can't be a normal result, since that
            /// embeds the output.
            ///
            /// # Safety
            /// To use this safely, ensure that you re-provide the lifetime to the state with
            /// [`Self::unerase_lifetime`], after it has been used in calls, within a scope, and
            /// before the lifetime of the input would expire.
            ///
            /// This uses a similar safety mechanism to [`LtErasedIPConsumed::erase_lifetime`],
            /// except that internally, it uses an option, such that as long as you uphold the
            /// safety contracts of [`Self::erase_lifetime`] and [`Self::unerase_lifetime`], then
            /// users of this safe state can't mess up without unsafe.
            pub unsafe fn erase_lifetime(initial_state: IPConsumed<'_, I>) -> Self {
                Self {
                    inner_state: Some(Ok(
                        // safety: see function contract
                        unsafe { LtErasedIPConsumed::erase_lifetime(initial_state) },
                    )),
                }
            }

            /// Safely modify a lifetime-erased result, producing an output.
            ///
            /// The output of this function has twofold components:
            /// * First, it is an [`Option`] - if this state currently does not have any actual
            ///   consumed or failed information, then  this function will return [`None`] - in
            ///   this case, you can panic, or perhaps return [`Poll::Pending`] and not progress
            ///   a future when there is no state.
            /// * Inside the option, there is a [`Poll`]. This is [`Poll::Pending`] in the case
            ///   that the internal function returns [`IPFailed`], and [`Poll::Ready`] with the
            ///   value in the case that it returns an [`IPConsumed`] with a value.
            #[inline]
            pub fn update<Q>(
                &mut self,
                transform: impl for<'i> FnOnce(
                    Result<IPConsumed<'i, I>, IPFailed<'i, I>>,
                ) -> IPResult<'i, I, Q>,
            ) -> Option<Poll<Q>> {
                // SAFETY
                // This object can only be used with the erase_lifetime and unerase_lifetime
                // functions, which require acknowledging the contract and, inside the implementation, the Option
                // can then only be Some() if there is a valid state.
                //
                // Because the function must work for all lifetimes, it means that the input slice
                // contained in the output must be derived from the input slice provided in the
                // input (or be 'static), for the function to actually be valid. Ergo, using the
                // pointers in the output is ok.
                if let Some(inner_state) = self.inner_state.take() {
                    // Unerase lifetime, apply transform, then reerase lifetime :)
                    let (maybe_out, new_result_state) = match inner_state {
                        Ok(consumed_erased) => {
                            transform(Ok(unsafe { consumed_erased.unerase_lifetime() }))
                        }
                        Err(already_failed) => {
                            transform(Err(unsafe { already_failed.unerase_lifetime() }))
                        }
                    }
                    .trans(|v| match v {
                        Ok((output, consumed)) => (
                            Poll::Ready(output),
                            Ok(unsafe { LtErasedIPConsumed::erase_lifetime(consumed) }),
                        ),
                        Err(failed) => (
                            Poll::Pending,
                            Err(unsafe { LtErasedIPFailed::erase_lifetime(failed) }),
                        ),
                    });
                    self.inner_state = Some(new_result_state);
                    Some(maybe_out)
                } else {
                    None
                }
            }

            #[inline]
            /// Give back the result with a given lifetime.
            ///
            /// This might return [`None`] if between erasing the lifetime of the data and
            /// unerasing it, some other function has taken it out of, say, a cell, and forgot to
            /// return it to the cell again with modification.
            ///
            /// # Safety
            /// Only safe if the input is valid for the given lifetime. See
            /// [`Self::erase_lifetime`] for more details. You should be safe if you use a variable
            /// that exists for longer then the current scope, and call [`Self::erase_lifetime`] at
            /// the start of the scope and [`Self::unerase_lifetime`] at the end of it.
            pub unsafe fn unerase_lifetime<'lifetime>(
                self,
            ) -> Option<Result<IPConsumed<'lifetime, I>, IPFailed<'lifetime, I>>> {
                self.inner_state
                    .map(|lt_erased_inner| match lt_erased_inner {
                        Ok(c) => Ok(
                            /* Safety: see function docs */
                            unsafe { c.unerase_lifetime::<'lifetime>() },
                        ),
                        Err(f) => Err(
                            /* Safety: see function docs */
                            unsafe { f.unerase_lifetime::<'lifetime>() },
                        ),
                    })
            }

            #[inline]
            /// Returns if there's actually state in here.
            pub fn is_empty(&self) -> bool {
                self.inner_state.is_none()
            }
        }

        /// Means of maintaining state between [`super::IPOperation`] interruptions.
        ///
        /// This trait is vital to being able to use [`Future`][fut]s as a means to construct
        /// partial input operations. It encapsulates a generic means of sharing state between
        /// futures and modifying state between and during multiple calls to `poll`.
        ///
        /// In essence, it captures a [`Cell`][cell]-like interface to allow for generic (perhaps
        /// multithreaded) synchronisation primitives.
        ///
        /// [fut]: `core::future::Future`
        /// [cell]: `core::cell::Cell`
        pub trait IContext<I> {
            /// Replace the current value with a new value, returning the old one.
            fn input_context_replace(
                &self,
                new_state: SafeIPResultPollstate<I>,
            ) -> SafeIPResultPollstate<I>;
        }

        impl<I, T: IContext<I>> IContext<I> for &T {
            #[inline]
            fn input_context_replace(
                &self,
                new_state: SafeIPResultPollstate<I>,
            ) -> SafeIPResultPollstate<I> {
                (*self).input_context_replace(new_state)
            }
        }

        /// Extension trait for [`IContext`]
        pub trait IContextExt<I>: IContext<I> {
            /// Update the input context. While this is running, this replaces the inner state with
            /// [`SafeIPResultPollstate<I>::default()`], which is empty.
            ///
            /// The passed function must return the modified state, and it can also provide an
            /// extra result that is returned as output to this function.
            ///
            /// Bear in mind that the modification function passed in may also receive an empty state,
            /// which can be reacted to as desired, in the case that the state has not currently been
            /// initialised or that the state is currently held by another thread if this state is
            /// synchronised between multiple.
            ///
            /// When the inner state is replaced again, whatever state was currently held at that
            /// time is not discarded if it's not empty.
            ///
            /// In a single threaded program, unless a modified function does creative things with
            /// [`IContext::input_context_replace`], the state will not be updated from it's default,
            /// empty condition until the modified current state is put back into it.
            ///
            /// In a multithreaded program with synchronised state between threads, though, it may
            /// be possible for another thread to put important state between the start and end of
            /// the `modifier` function. In such a case that the state is not empty, the
            /// `state_was_filled` function is called with a reference to self and the non-empty
            /// state.
            #[inline]
            fn input_context_update<R>(
                &self,
                modifier: impl FnOnce(SafeIPResultPollstate<I>) -> (SafeIPResultPollstate<I>, R),
                non_empty_intermediary_state: impl FnOnce(&Self, SafeIPResultPollstate<I>),
            ) -> R {
                let current_state = self.input_context_replace(SafeIPResultPollstate::default());
                let (new_state, f_result) = modifier(current_state);
                let intermediary_state = self.input_context_replace(new_state);
                if !intermediary_state.is_empty() {
                    non_empty_intermediary_state(self, intermediary_state);
                }
                f_result
            }
        }

        impl<I, T: IContext<I>> IContextExt<I> for T {}

        /// The core structure for enabling ergonomic construction of complex, partial
        /// parser/reactors.
        ///
        /// This turns any [`super::IPOperation`] into composable futures that carry around an [`IContext`] that allows
        /// an outer wrapper to provide them with input and output.
        ///
        /// Provide multiple futures with the same context in a larger `async fn`, and you will
        /// create a state machine, where each completed future will modify the stored state to
        /// consume input it used, and leave the rest for the next state.
        ///
        /// Using loops and conditionals allows you to construct arbitrary complex state machines.
        ///
        /// # Error Handling
        /// In the case that no state is provided, the future will poll.
        ///
        /// In the case that the previous state has not been turned from a failed/incomplete
        /// consumption back into an initial state, it will poll as well and return
        /// the original failure mode unchanged.
        ///
        /// In the case that, during the period in which the inner state is being updated some
        /// other thread or function *updates* the state to not be empty, this will *panic*.
        #[must_use = "This is a state machine future you made, so use it to build things :), it will do nothing without `.await`ing"]
        pub struct IPOperationFuture<'state, I, ICtx: IContext<I>, InnerOp: super::IPOperation<I>> {
            /// The context used to provide input to this future.
            context: &'state ICtx,
            /// The inner, partial operation that can consume some input.
            inner_operation: InnerOp,
            _pi: PhantomData<&'state [I]>,
        }

        impl<'state, I, ICtx: IContext<I>, InnerOp: super::IPOperation<I>>
            IPOperationFuture<'state, I, ICtx, InnerOp>
        {
            #[inline]
            /// Create a new future that uses the provided context to communicate partial input to
            /// the provided operation.
            pub fn new(context: &'state ICtx, operation: InnerOp) -> Self {
                Self {
                    context,
                    inner_operation: operation,
                    _pi: PhantomData,
                }
            }

            /// Extract both the context and the inner operation.
            #[inline]
            pub fn pin_project(self: Pin<&mut Self>) -> (&ICtx, Pin<&mut InnerOp>) {
                // SAFETY: no other unsafe code provides the inner operation unpinned, and the
                // context is a shared reference anyway which can always be extracted from Pin
                // safely even without `Unpin`
                let bare_mut_self = unsafe { self.get_unchecked_mut() };
                (bare_mut_self.context, unsafe {
                    Pin::new_unchecked(&mut bare_mut_self.inner_operation)
                })
            }
        }

        // The most important part... Future!
        impl<'state, I, ICtx: IContext<I>, InnerOp: super::IPOperation<I>> Future
            for IPOperationFuture<'state, I, ICtx, InnerOp>
        {
            type Output = InnerOp::Output;

            #[inline]
            fn poll(self: Pin<&mut Self>, _cx: &mut core::task::Context<'_>) -> Poll<Self::Output> {
                // Extract the input state and operation
                let (ctx, op) = self.pin_project();
                ctx.input_context_update(|mut inner_state| {
                    let polling_output = inner_state
                        .update(|curr_state| {
                            curr_state
                                .and_then(|input_data| op.try_progress_partial_input(input_data))
                        })
                        .unwrap_or(Poll::Pending); // In the case of no available state, we *pend*
                    (inner_state, polling_output)
                }, |_state, _nonempty_inner_state| panic!("Input Partial-Operation Future could not update state because it already had some other state inside!"))
            }
        }
    }

    /// When defining state machines as [`Future`][fut]s, these futures must be externally driven,
    /// to provide and access internal state outside of the default interface provided by [`Future`][fut]s,
    /// and return more that just `Poll::Pending` or `Poll::Ready`.
    ///
    /// [fut]: `core::future::Future`
    pub mod drivers {
        use core::{
            future::Future,
            marker::PhantomData,
            pin::Pin,
            task::{Context, Poll},
        };

        use chain_trans::Trans;

        use super::{
            pollstate::{IContext, SafeIPResultPollstate},
            IPConsumed, IPFailed, IPResult,
        };

        /// Type that can *either* be (paused waiting for more input or successful with output), or
        /// be paused for reasons other than insufficient input.
        ///
        /// See [`IDriver::maybe_pinned_idrive_select_with_async_ctx`] for information on exactly
        /// why it is this strange combination and how it relates to async runtimes or different
        /// future implementations.
        pub type MaybeIPResult<'buf, I, Output> =
            Result<IPResult<'buf, I, Output>, IPConsumed<'buf, I>>;

        pub mod prelude {
            pub use super::{IDriver, IDriverExt};
        }

        /// Something that can be driven by input until it produces a *single* value, by input only.
        ///
        /// Note that, drivable tasks can have multiple channels of input, selected for which is
        /// provided to. In particular, this allows something to process multiple streams of input
        /// - using futures that only respond to one of the stored references and combining them
        /// with something that will poll all of them.
        pub trait IDriver<I> {
            /// Final, single output of this driven input-only partial parser.
            type Output;
            /// The state that holds all of the input streams.
            type InputContexts;

            /// This *tries* to drive the driver forward, with the provided input, on the selected
            /// input context.
            ///
            /// For a correctly implemented pollstate future using only the futures defined in this
            /// crate, the state must be *either* an `OK` [`IPResult`] with output value obtained
            /// from a `Poll::Ready`, *or* it must be an `Err` [`IPResult`] paired with a
            /// `Poll::Pending` state, because a pure pollstate future constructed using only the
            /// items in this library cannot be in the state `Poll::Pending` unless it has
            /// incomplete input, unless the future takes input from *multiple* input contexts, as
            /// well, in which case it could be blocked by any of them (or perhaps the output
            /// contexts too).
            ///
            /// HOWEVER, if a future embeds futures that don't work on an input context exclusively
            /// to determine continuation - for instance using futures from other async runtimes - they may
            /// enter a pending state even when there was sufficient input up until the point of
            /// interruption.
            ///
            /// That is, in the case where the `poll` function of some internal
            /// future returns `Poll::Pending`, but the internal state is left as `Ok` with some
            /// amount of input left unconsumed, it means that an *external* future from *another*
            /// runtime is being waited upon.
            ///
            /// IF the internal state is left as incomplete, and yet the future returns
            /// Poll::Ready, that would indicate that some other future was used in such a way as
            /// to return a result. This probably comes down to some kind of multiple interleaved `poll`
            /// calls inside one of the futures. In such a case, we take out how much was consumed
            /// and return an Ok().
            ///
            /// This function, then, returns *either* `OK` with the parsing progress information
            /// encoded as an [`IPResult`], *or* it indicates that this driver is currently waiting
            /// on a future from another runtime or otherwise is interrupted for reasons other than
            /// the input being insufficient to run to completion.
            ///
            /// Most end-users probably want to use one of the simpler functions in the
            /// [`IDriverExt`] trait, to avoid things like passing down async contexts, etc.
            ///
            /// This should never be called any more once a complete value is returned.
            fn maybe_pinned_idrive_select_with_async_ctx<'buf, ICtx: IContext<I>>(
                self: Pin<&mut Self>,
                context_selector: impl for<'c> FnOnce(&'c Self::InputContexts) -> &'c ICtx,
                input: &'buf [I],
                async_ctx: Context<'_>,
            ) -> MaybeIPResult<'buf, I, Self::Output>;
        }

        /// Extension trait for generic driver items.
        ///
        /// Note that, for *all* items in this trait, you should never call the functions again
        /// once a final output has been produced.
        pub trait IDriverExt<I>: IDriver<I> {
            /// Like [`IDriver::maybe_pinned_idrive_select_with_async_ctx`], but provides a dummy
            /// waker. This function (theoretically) makes futures of existing runtimes unusable in
            /// your inner, driven futures. In practise, this is probably fine, because the idea of
            /// this library is defining parsers and reactors in a way independent of the runtime.
            /// Futures from this library do not use wakers at all.
            ///
            /// See [`IDriver::maybe_pinned_idrive_select_with_async_ctx`] for information on the other
            /// parameters and why the result type is a bit odd.
            #[inline]
            fn maybe_pinned_idrive_select<'buf, ICtx: IContext<I>>(
                self: Pin<&mut Self>,
                context_selector: impl for<'c> FnOnce(&'c Self::InputContexts) -> &'c ICtx,
                input: &'buf [I],
            ) -> MaybeIPResult<'buf, I, Self::Output> {
                let w = dummy_waker::dummy_waker();
                let ctx = Context::from_waker(&w);
                self.maybe_pinned_idrive_select_with_async_ctx(context_selector, input, ctx)
            }

            /// Like [`Self::maybe_pinned_idrive_select`], but does not require pinning - in particular,
            /// anything where we are unpinned will let us drive ourselves more easily.
            ///
            /// This allows you to select an input stream without an async context. See
            /// [`IDriver::maybe_pinned_idrive_select_with_async_ctx`] for info on the weird
            /// return type.
            ///
            /// It is particularly relevant in the case that you have written a drivable future
            /// that returns both input and output.
            #[inline]
            fn maybe_idrive_select<'buf, ICtx: IContext<I>>(
                &mut self,
                context_selector: impl for<'c> FnOnce(&'c Self::InputContexts) -> &'c ICtx,
                input: &'buf [I],
            ) -> MaybeIPResult<'buf, I, Self::Output>
            where
                Self: Unpin,
            {
                let pinned_self = Pin::new(self);
                pinned_self.maybe_pinned_idrive_select(context_selector, input)
            }

            /// Like [`Self::maybe_pinned_idrive_select`], except the input context collection is
            /// just something that *is* an input context and can be immediately used as-is
            #[inline]
            fn maybe_pinned_idrive<'buf>(
                self: Pin<&mut Self>,
                input: &'buf [I],
            ) -> MaybeIPResult<'buf, I, Self::Output>
            where
                Self::InputContexts: IContext<I>,
            {
                self.maybe_pinned_idrive_select(|v| v, input)
            }

            /// Like [`Self::maybe_pinned_idrive`], but available when we are Unpin
            #[inline]
            fn maybe_idrive<'buf>(
                &mut self,
                input: &'buf [I],
            ) -> MaybeIPResult<'buf, I, Self::Output>
            where
                Self::InputContexts: IContext<I>,
                Self: Unpin,
            {
                self.maybe_idrive_select(|v| v, input)
            }

            /// All of the `maybe_*` functions in this trait account for the possibility that the
            /// state machine may be interrupted or stop for reasons *other* than that it needs
            /// more input from the input context selected by the state of this driver.
            ///
            /// However, with *good library design* you should hardly ever actually work with
            /// futures that use runtime-dependent futures, instead, ideally, they should not need
            /// an external runtime at all and can be driven synchronously or asynchronously.
            ///
            /// What this function does is *assume* that the inner future cannot pause for any
            /// reason other than that there is insufficient input to the input context, panicking
            /// if this is not the case. This assumption does not hold in the following cases:
            /// * Your future has *multiple* input contexts - in which case you should poll them all
            ///   the future may be blocked from progress by any of them - this is solved by having
            ///   this function only present on the case where the state is itself already an input
            ///   context.
            /// * Your future has a single *output* context it is waiting to emit from. The common
            ///   case of an input-output pair is handled in the [`crate::bidi`] module, having
            ///   convenience methods equivalent to this where a future driver may have exactly one
            ///   of each of an input context and an output context.
            /// * Your future has multiple output contexts it may wait from.
            /// * And of course, the case where a future that does not actually stop only when
            ///   there is input needed, as may be the case if a future from another runtime is
            ///   used.
            #[inline]
            fn pure_pinned_idrive<'buf>(
                self: Pin<&mut Self>,
                input: &'buf [I],
            ) -> IPResult<'buf, I, Self::Output>
            where
                Self::InputContexts: IContext<I>,
            {
                match self.maybe_pinned_idrive(input) {
                    Ok(v) => v,
                    Err(_) => panic!("Drove input with assumption that a future could only be paused due to lack of input, this assumption was wrong")
                }
            }

            #[inline]
            /// Just like [`Self::pure_pinned_idrive`], but convenient for the case where we need
            /// no pinning.
            ///
            /// If you have a future that has a single input and single output context, use
            /// [`crate::bidi`] for convenience instead.
            fn pure_idrive<'buf>(&mut self, input: &'buf [I]) -> IPResult<'buf, I, Self::Output>
            where
                Self: Unpin,
                Self::InputContexts: IContext<I>,
            {
                Pin::new(self).pure_pinned_idrive(input)
            }
        }

        impl<I, T: IDriver<I>> IDriverExt<I> for T {}

        /// Something that is driven by input until it produces a single value and excess input
        /// left over.
        ///
        /// Note that the state has to be provided as a reference. This is because it is not...
        /// clean to take it as value due to pinning weirdness.
        pub struct IDriverFut<'state, I, IState: 'state, IFuture: Future> {
            state: &'state IState,
            fut: IFuture,
            _pd: PhantomData<&'state I>,
        }

        impl<'state, I, IState: 'state, IFuture: Future> IDriverFut<'state, I, IState, IFuture> {
            /// Create a driven future using the existing state as a parameter. This driver is an *input only* driver
            /// i.e. it can't produce any output other than it's special value. However, it can
            /// still require multiple streams of input if so desired.
            ///
            /// This future should not take state from any input context for modification without
            /// putting it back.
            #[inline]
            pub fn make_driver_with_state(
                state: &'state IState,
                future_builder: impl FnOnce(&'state IState) -> IFuture,
            ) -> Self {
                let f = future_builder(state);
                Self {
                    state,
                    fut: f,
                    _pd: PhantomData,
                }
            }

            /// Get relevant chunks of data from Pin<mut> of self
            #[inline]
            pub fn pin_project(self: Pin<&mut Self>) -> (&IState, Pin<&mut IFuture>) {
                // Safety: no other implementation from pinned self produces unpinned reference to
                // future.
                let raw_self_mut = unsafe { self.get_unchecked_mut() };
                (raw_self_mut.state, unsafe {
                    Pin::new_unchecked(&mut raw_self_mut.fut)
                })
            }
        }

        impl<'state, I, IState: 'state, IFuture: Future> IDriver<I>
            for IDriverFut<'state, I, IState, IFuture>
        {
            type Output = IFuture::Output;

            type InputContexts = IState;

            #[inline]
            fn maybe_pinned_idrive_select_with_async_ctx<'buf, ICtx: IContext<I>>(
                self: Pin<&mut Self>,
                context_selector: impl for<'c> FnOnce(&'c Self::InputContexts) -> &'c ICtx,
                input: &'buf [I],
                mut async_ctx: Context<'_>,
            ) -> MaybeIPResult<'buf, I, Self::Output> {
                // Create a new unconsumed input state, lifetime-erase it, and dump it right there.
                let initial_input = IPConsumed {
                    ate: 0,
                    remaining: input,
                };
                let (input_context_state, pollstate_inner_future) = self
                    .pin_project()
                    .trans(|(istate, future)| (context_selector(istate), future));

                // Safety - this is paired with an unerase before the input expires.
                let old_state = input_context_state.input_context_replace(unsafe {
                    SafeIPResultPollstate::erase_lifetime(initial_input)
                });
                // Poll the future!
                let future_poll_result = pollstate_inner_future.poll(&mut async_ctx);
                // Retrieve the new state
                //
                // Safety - this is paired directly with with the first erasure.
                //
                // Note that the inner future deleting the state entirely when provided with some
                // initial state is considered a programming error - a non-empty state *is*
                // provided by the initial input.
                let intermediary_result_state = input_context_state
                    .input_context_replace(old_state)
                    .trans(|updated_state| unsafe { updated_state.unerase_lifetime::<'buf>() })
                    .expect(
                        "Your future took the transitive per-poll state and didn't put it back.",
                    );

                match (future_poll_result, intermediary_result_state) {
                    (Poll::Pending, Err(failed_and_needs)) => Ok(Err(failed_and_needs)),
                    (Poll::Ready(final_result_value), Ok(consumed_v)) => {
                        Ok(Ok((final_result_value, consumed_v)))
                    }
                    // Clearly it doesn't actually need more input if it output a value lol
                    (
                        Poll::Ready(final_result_value),
                        Err(IPFailed {
                            consumed,
                            needs: _,
                            suggested: _,
                        }),
                    ) => Ok(Ok((final_result_value, consumed))),
                    // Some other future has caused a pause without producing a value.
                    (Poll::Pending, Ok(consumed)) => Err(consumed),
                }
            }
        }
    }
}

// pare
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
