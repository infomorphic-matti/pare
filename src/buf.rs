//! Module enabling buffering of input.
//!
//! # THE BUFFERING PROBLEM
//! In this crate, there is the ability to define both *atomic* operations, and *partial*
//! operations.
//!
//! *Atomic* operations work on a single chunk of contiguous memory - a slice. They can, in fact,
//! *borrow* from that slice, to enable zero-copy parsing.
//!
//! *Partial* operations work on arbitrary small chunks of contiguous input, holding state in a
//! Future or other miscellaneous state machine. During interruptions, these partial operations are
//! incapable of holding borrowed input data, because the lifetime of the input data may be shorter
//! than the lifetime of the state machine.
//!
//! This means that without some means of integrating atomic operations into partial operations,
//! there is no way to handle dynamic-length inputs.
//!
//! ## How This Is Handled In Std Environments
//! In an environment that isn't embedded, you'd typically handle this by allocating on the heap,
//! using something like `Vec` to ensure memory contiguity. However, this has a number of issues in
//! the case of embedded environments and protocol handlers:
//! * It can be hard to constrain total memory usage like this - there is only weak control of how
//!   much memory may be consumed, and for potentially security sensitive code that cannot globally
//!   crash, this is unsuitable.
//! * Especially in the case of having multiple buffers for separate atomic operations, passing
//!   around a single vector may cause lifetime issues where one atomic operation holds a borrow to
//!   the vector preventing further expansion to buffer input for a second atomic operation, if both
//!   are within a partial operation that must pause between each of these atomic operations.
//! * If using multiple buffers to counteract the previous issue, it leads to a risk of significant
//!   waste of contiguous memory as each buffer over allocates.
//! * If there is no global allocator, using stack space instead leaves issues where it can be
//!   difficult to reclaim over-allocated memory in such a case where you essentially hold a
//!   reference to a bare array.
//!
//! In essence, trying to wrangle an interface to provide buffers is difficult, because
//! higher-level abstractions don't provide an easy way to limit memory usage, and a global
//! allocator may not be available.
//!
//! The capabilities actually necessary for an input buffer to which references can be held:
//! * Operations which end up copying to the stack should not prevent deallocation of memory from
//!   the buffer.
//! * It should be able to provide contiguous chunks of memory suitable for storing one input type,
//!   either up to a fixed limit or allocated to expand in the presence of a global allocator.
//! * When memory is provided by reference, it should be able to recover excess memory in such a
//!   way that it can be reused contiguously.
//! * The lifetime of the buffer should be propagatable to the output, such that the output type
//!   does not outlive whatever chunk of buffer it was given, if it actually uses it. This problem
//!   can also be solved by being able to separate a buffer into separately owned chunks and using
//!   a crate like `yoke`. to attach a buffer to a zerocopy or atomic parser holding it as a reference.
//!
//! Unfortunately, we have to avoid `alloc` primitives like `Vec` for this, because it seems
//! unlikely that the sort of specialised efficient split operation is unlikely to get put as part
//! of the allocator API: <https://github.com/rust-lang/wg-allocators/issues/105> seems to have poor reception
//! (for some good reasons).

// pare
// Copyright (C) 2022  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
